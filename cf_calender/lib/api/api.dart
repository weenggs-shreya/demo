import 'package:dio/dio.dart';

class DioClient {
  final Dio dio = Dio();
  static const baseUrl = "https://api-beta.contractorforeman.net";
  static const postsEndPoint = baseUrl + "/service.php";
}