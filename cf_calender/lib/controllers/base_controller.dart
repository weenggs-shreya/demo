import 'package:get/get.dart';

class BaseController extends GetxController{
  final _isLoading = false.obs;
  bool get isLoading => _isLoading.value;

  showLoading(){
    _isLoading.value=true;
  }
  hideLoading(){
    _isLoading.value=false;
  }


}