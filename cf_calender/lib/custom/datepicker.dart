import 'package:cf_calender/res/color.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rounded_date_picker/flutter_rounded_date_picker.dart';
import 'package:intl/intl.dart';

DateTime selectedDate = DateTime.now();

selectDate(BuildContext context, TextEditingController controller) async {
  DateTime? newDateTime = await showRoundedDatePicker(
      context: context,
      height: 400,
      textNegativeButton: "Cancel",
      initialDate: selectedDate != null ? selectedDate : DateTime.now(),
      firstDate: DateTime(1900),
      lastDate: DateTime(DateTime.now().year + 1),
      borderRadius: 0,
      theme: ThemeData(
        primaryColor: appColor.blue,
        accentColor: appColor.blue,
        textTheme: TextTheme(
          caption: TextStyle(color: appColor.darkgray),
        ),
      ),
      styleDatePicker: MaterialRoundedDatePickerStyle(
        paddingMonthHeader: EdgeInsets.all(30),
        marginLeftArrowPrevious: 16,
        marginTopArrowPrevious: 16,
        marginTopArrowNext: 16,
        marginRightArrowNext: 32,
        textStyleButtonPositive: TextStyle(
          color: appColor.blue,
        ),
        textStyleButtonNegative: TextStyle(color: appColor.blue),
      ));
  if (newDateTime != null) {
    final f = DateFormat('MM/dd/yyyy');
    selectedDate = newDateTime;
    // dateController1.datecontroller
    controller
      ..text = f.format(selectedDate)
      ..selection = TextSelection.fromPosition(TextPosition(
          offset: controller.text.length, affinity: TextAffinity.upstream));
  }
  // if (selectedDate != null) {
  //   final f = DateFormat('yyyy-MM-dd');
  //   selectedDate = selectedDate;
  //
  //   controller
  //     ..text = f.format(selectedDate)
  //     ..selection = TextSelection.fromPosition(TextPosition(
  //         offset: controller.text.length, affinity: TextAffinity.upstream));
  // }
}
