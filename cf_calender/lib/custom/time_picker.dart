import 'package:cf_calender/res/color.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rounded_date_picker/flutter_rounded_date_picker.dart';
import 'package:intl/intl.dart';

DateTime? parsedTime;

seletecTime(BuildContext context, TextEditingController timeController) async {
  final timePicked = await showRoundedTimePicker(
    context: context,
    initialTime: TimeOfDay.now(),
    theme: ThemeData(
      primaryColor: appColor.blue,
      accentColor: appColor.blue,
      textTheme: TextTheme(
        caption: TextStyle(color: appColor.blue),
      ),
    ),
  );
  if (timePicked != null) {
    print(timePicked.format(context)); //output 10:51 PM
    parsedTime = DateFormat.jm().parse(timePicked.format(context).toString());

    // print(parsedTime); //output 1970-01-01 22:53:00.000
    String formattedTime = DateFormat('HH:mm').format(parsedTime!);
    // print(formattedTime);
    timeController.text=formattedTime;
    // setState(() {
    //   timeController.text = formattedTime; //set the value of text field.
    // }); //output 14:59:00

  } else {
    print("Time is not selected");
  }
}
