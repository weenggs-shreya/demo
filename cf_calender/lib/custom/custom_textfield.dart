import 'package:flutter/material.dart';

import '../res/color.dart';

class CustomTextField extends StatefulWidget {
  VoidCallback? callback;
  int? maxline;
  TextEditingController? controller;
  bool readOnly;
  FocusNode? focusNode;
  void Function(String?)? onsaved;

  CustomTextField(
      {this.callback,
      this.maxline,
      this.readOnly = false,
      this.controller,
      this.onsaved,
      this.focusNode});

  @override
  State<CustomTextField> createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      // keyboardType: TextInputType.multiline,
      focusNode: widget.focusNode,
      maxLines: widget.maxline,
      readOnly: widget.readOnly,
      controller: widget.controller,
      onSaved: widget.onsaved,
      decoration: InputDecoration(
        isDense: true,
        contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: appColor.darkgray),
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: appColor.darkgray),
        ),
      ),
      onTap: widget.callback,
    );
  }
}
