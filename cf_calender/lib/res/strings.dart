Strings strings = Strings();

final title = "Strings";

class Strings {
  static final Strings _strings = Strings._i();

  factory Strings() {
    return _strings;
  }

  Strings._i();

  final String calender = 'Calender';
  final String module = 'Module';
  final String employees = 'Employees';
  final String display_option = 'Display Options';
  final String event_detail = 'Event Details';
  final String cancel = 'Cancel';
  final String save = 'Save';
  final String start_date = 'Start Date';
  final String start_time = 'Start Time';
  final String event_name = 'Event Name';
  final String description = 'Description';
  final String end_date = 'End Date';
  final String end_time = 'End Time';
  final String loaction = 'Location';
}
