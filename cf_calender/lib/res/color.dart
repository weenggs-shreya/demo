import 'package:flutter/material.dart';

AppColor appColor = AppColor();

class AppColor {
  static final AppColor _appColor = AppColor._i();

  factory AppColor() {
    return _appColor;
  }

  AppColor._i();

  final Color white = Color(0xFFffffff);
  final Color black = Color(0xFF000000);
  final Color grey = Color(0xffEBECF0);
  final Color darkgrey = Color(0xff9E9E9E);
  final Color blue = Color(0xff01579B);
  final Color green = Color(0xff4CAF50);
  final Color darkgray = Color(0xff9E9E9E);
  final Color darkgray1 = Color(0xff616161);
  final Color transparent = Colors.transparent;
  final Color purpleAccest = Color(0xFF6A1B97);
  final Color red = Color(0xFFF44336);
  final Color Cyan = Color(0xFF80DEEA);
  final Color Cream = Color(0xFFF7D8BA);
  final Color Yellow = Color(0xFFFFFF8D);
  final Color lightBlue = Color(0xFF2196F3);

//FontSize
  static const double font_17 = 17;
  static const double font_15 = 15;
  static const double font_12 = 12;
  static const double font_13 = 13;
}
