import 'package:cf_calender/res/color.dart';
import 'package:cf_calender/src/pages/date_time_selector.dart';
import 'package:cf_calender/src/pages/extension.dart';
import 'package:flutter/material.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import '../custom/custom_textfield.dart';
import '../custom_calender/lib/calendar_view.dart';
import '../res/strings.dart';
import '../src/pages/event.dart';

class AddEventWidget extends StatefulWidget {
  final void Function(CalendarEventData<Event>)? onEventAdd;
  final bool withDuration;

  const AddEventWidget({Key? key, this.onEventAdd, this.withDuration = false})
      : super(key: key);

  @override
  _AddEventWidgetState createState() => _AddEventWidgetState();
}

class _AddEventWidgetState extends State<AddEventWidget> {
  late DateTime _startDate;
  late DateTime _endDate;

  DateTime? _startTime;

  DateTime? _endTime;

  String _title = "";

  String _description = "";

  Color _color = Colors.blue;

  late FocusNode _titleNode;

  late FocusNode _descriptionNode;

  late FocusNode _dateNode;

  final GlobalKey<FormState> _form = GlobalKey();

  late TextEditingController _startDateController;
  late TextEditingController _startTimeController;
  late TextEditingController _endTimeController;
  late TextEditingController _endDateController;

  @override
  void initState() {
    super.initState();

    _titleNode = FocusNode();
    _descriptionNode = FocusNode();
    _dateNode = FocusNode();

    _startDateController = TextEditingController();
    _endDateController = TextEditingController();
    _startTimeController = TextEditingController();
    _endTimeController = TextEditingController();
  }

  @override
  void dispose() {
    _titleNode.dispose();
    _descriptionNode.dispose();
    _dateNode.dispose();

    _startDateController.dispose();
    _endDateController.dispose();
    _startTimeController.dispose();
    _endTimeController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _form,
      child: Scaffold(
        appBar: AppBar(
          elevation: 0,
          backgroundColor: appColor.blue,
          centerTitle: true,
          leading: TextButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Text(
              strings.cancel,
              style: TextStyle(color: appColor.white, fontSize: 14),
            ),
          ),
          actions: [
            TextButton(
              onPressed: () {
                print("Fdf");
                _createEvent();
              },
              child: Text(
                strings.save,
                style: TextStyle(color: appColor.white, fontSize: 15),
              ),
            ),
          ],
          title: Text(
            strings.event_detail,
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
        ),
        body: SingleChildScrollView(
          child: Container(
            margin: EdgeInsets.all(5),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: Container(
                        margin: EdgeInsets.only(left: 3, right: 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              strings.start_date,
                            ),
                            DateTimeSelectorFormField(
                              controller: _startDateController,
                              validator: (value) {
                                if (value == null || value == "")
                                  return "Please select start time.";

                                return null;
                              },
                              onSave: (date) => _startDate = date,
                              textStyle: TextStyle(
                                color: appColor.black,
                                fontSize: 17.0,
                              ),
                              type: DateTimeSelectionType.date,
                            ),
                          ],
                        ),
                      ),
                    ),
                    Expanded(
                      child: Container(
                        margin: EdgeInsets.only(left: 3, right: 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              strings.start_time,
                            ),
                            DateTimeSelectorFormField(
                              controller: _startTimeController,
                              validator: (value) {
                                if (value == null || value == "")
                                  return "Please select start time.";

                                return null;
                              },
                              onSave: (date) => _startTime = date,
                              textStyle: TextStyle(
                                color: appColor.black,
                                fontSize: 17.0,
                              ),
                              type: DateTimeSelectionType.time,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Text(strings.event_name),
                CustomTextField(
                  // controller: eventNameController,
                  onsaved: (value) => _title = value?.trim() ?? "",
                ),
                SizedBox(
                  height: 10,
                ),
                Text(strings.description),
                CustomTextField(
                  maxline: 5,
                  // controller: descriptionController,
                  focusNode: _descriptionNode,
                  onsaved: (value) => _description = value?.trim() ?? "",
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: Container(
                        margin: EdgeInsets.only(left: 3, right: 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              strings.end_date,
                            ),
                            DateTimeSelectorFormField(
                              controller: _endDateController,
                              validator: (value) {
                                if (value == null || value == "")
                                  return "Please select start time.";

                                return null;
                              },
                              onSave: (date) => _endDate = date,
                              textStyle: TextStyle(
                                color: appColor.black,
                                fontSize: 17.0,
                              ),
                              type: DateTimeSelectionType.date,
                            ),
                          ],
                        ),
                      ),
                    ),
                    Expanded(
                      child: Container(
                        margin: EdgeInsets.only(left: 3, right: 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              strings.end_time,
                            ),
                            DateTimeSelectorFormField(
                              controller: _endTimeController,
                              validator: (value) {
                                if (value == null || value == "")
                                  return "Please select start time.";

                                return null;
                              },
                              onSave: (date) => _endTime = date,
                              textStyle: TextStyle(
                                color: appColor.black,
                                fontSize: 17.0,
                              ),
                              type: DateTimeSelectionType.time,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
                Row(
                  children: [
                    Text(
                      "Event Color: ",
                      style: TextStyle(
                        color: appColor.black,
                        fontSize: 17,
                      ),
                    ),
                    GestureDetector(
                      onTap: _displayColorPicker,
                      child: CircleAvatar(
                        radius: 15,
                        backgroundColor: _color,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _createEvent() {
    if (!(_form.currentState?.validate() ?? true)) return;

    _form.currentState?.save();

    final event = CalendarEventData<Event>(
      date: _startDate,
      color: _color,
      endTime: _endTime,
      startTime: _startTime,
      description: _description,
      endDate: _endDate,
      title: _title,
      event: Event(
        title: _title,
      ),
    );

    widget.onEventAdd?.call(event);
    _resetForm();
  }

  void _resetForm() {
    _form.currentState?.reset();
    _startDateController.text = "";
    _endTimeController.text = "";
    _startTimeController.text = "";
  }

  void _displayColorPicker() {
    var color = _color;
    showDialog(
      context: context,
      useSafeArea: true,
      barrierColor: Colors.black26,
      builder: (_) => SimpleDialog(
        clipBehavior: Clip.hardEdge,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(30.0),
          side: BorderSide(
            color: appColor.green,
            width: 2,
          ),
        ),
        contentPadding: EdgeInsets.all(20.0),
        children: [
          Text(
            "Event Color",
            style: TextStyle(
              color: appColor.black,
              fontSize: 25.0,
            ),
          ),
          Container(
            margin: const EdgeInsets.symmetric(vertical: 20.0),
            height: 1.0,
            color: appColor.green,
          ),
          ColorPicker(
            displayThumbColor: true,
            enableAlpha: false,
            pickerColor: _color,
            onColorChanged: (c) {
              color = c;
            },
          ),
          Center(
            child: Padding(
              padding: EdgeInsets.only(top: 50.0, bottom: 30.0),
              child: GestureDetector(
                child: Text("Select"),
                onTap: () {
                  if (mounted)
                    setState(() {
                      _color = color;
                    });
                  context.pop();
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}
