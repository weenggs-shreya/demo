
import 'package:flutter/material.dart';
import '../pages/event.dart';
import 'month_view_widget.dart';

class MonthViewPageDemo extends StatefulWidget {
  const MonthViewPageDemo({
    Key? key,
  }) : super(key: key);

  @override
  _MonthViewPageDemoState createState() => _MonthViewPageDemoState();
}

class _MonthViewPageDemoState extends State<MonthViewPageDemo> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        elevation: 8,
        // onPressed: _addEvent,
        onPressed: (){},
      ),
      body: MonthViewWidget(),
    );
  }

  // Future<void> _addEvent() async {
  //   final event = await context.pushRoute<CalendarEventData<Event>>(
  //     CreateEventPage(
  //       withDuration: true,
  //     ),
  //   );
  //   if (event == null) return;
  //   CalendarControllerProvider.of<Event>(context).controller.add(event);
  // }
}
