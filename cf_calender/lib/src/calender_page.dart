import 'package:cf_calender/res/color.dart';
import 'package:cf_calender/src/add_event_page.dart';
import 'package:cf_calender/src/pages/extension.dart';
import 'package:cf_calender/src/pages/month_view_widget.dart';
import 'package:flutter/material.dart';
import '../custom_calender/lib/calendar_view.dart';
import '../res/strings.dart';
import '../src/pages/event.dart';
import 'create_event_page.dart';

DateTime get _now => DateTime.now();

class CalenderPage extends StatefulWidget {
  const CalenderPage({Key? key}) : super(key: key);

  @override
  State<CalenderPage> createState() => _CalenderPageState();
}

class _CalenderPageState extends State<CalenderPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: appColor.blue,
          title: Text(strings.calender),
          centerTitle: true,
        ),
        body:
            // MonthViewWidget(),

            SingleChildScrollView(
          physics: NeverScrollableScrollPhysics(),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: Container(
                      margin: EdgeInsets.only(left: 3, right: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            strings.module,
                            style: TextStyle(fontSize: 20),
                          ),
                          TextFormField(
                            onTap: () {
                              print("Module...");
                            },
                            readOnly: true,
                            decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide:
                                    BorderSide(color: appColor.darkgray),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide:
                                    BorderSide(color: appColor.darkgray),
                              ),
                              hintText: 'Display Option',
                              suffixIcon: Icon(
                                Icons.arrow_drop_down,
                                color: appColor.darkgray,
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      margin: EdgeInsets.only(
                        left: 10,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            strings.employees,
                            style: TextStyle(fontSize: 20),
                          ),
                          TextFormField(
                            onTap: () {
                              print("Employe...");
                            },
                            decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide:
                                    BorderSide(color: appColor.darkgray),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide:
                                    BorderSide(color: appColor.darkgray),
                              ),
                              hintText: 'Display Option',
                              suffixIcon: Icon(
                                Icons.arrow_drop_down,
                                color: appColor.darkgray,
                              ),
                            ),
                            readOnly: true,
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              SingleChildScrollView(
                child: Container(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  color: appColor.white,
                  child: MonthViewWidget(),
                ),
              ),
            ],
          ),
        ),
        floatingActionButton: FloatingActionButton(
          backgroundColor: appColor.green,
          child: Icon(Icons.add, size: 40),
          elevation: 8,
          onPressed: _addEvent,
        ));
  }

  Future<void> _addEvent() async {
    final event = await context.pushRoute<CalendarEventData<Event>>(
      CreateEventPage(
        withDuration: true,
      ),
    );
    if (event == null) return;
    CalendarControllerProvider.of<Event>(context).controller.add(event);
  }
}
