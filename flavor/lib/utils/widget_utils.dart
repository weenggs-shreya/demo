import 'package:flutter/material.dart';
import 'package:multiscreen/multiscreen.dart';

import '../res/color.dart';

class WidgetUtils {
  static Widget spaceVertical(double height) {
    return SizedBox(height: resize(height));
  }

  static Widget spaceHorizontal(double width) {
    return SizedBox(width: resize(width));
  }

  static Widget buildTextField({
    required String hintText,
    required TextEditingController controller,
    Icon? prefixIcon,
    IconButton? suffixIcon,
    bool? isVisible,
    double? leftpadding,
    TextInputType? textInputType,
    double? borderRadius,
    Color? fillColor=Colors.white,
  }) {
    return TextFormField(
      keyboardType: textInputType,
      cursorColor: appColor.grey,
      controller: controller,
      obscureText: isVisible ?? false,
      style: TextStyle(
        color: appColor.grey,
        fontSize: 20,
        letterSpacing: 1,
      ),
      decoration: InputDecoration(
        border: InputBorder.none,
        hintText: hintText,
        hintStyle: TextStyle(
          color: appColor.grey,
          fontSize: 15,
        ),
        contentPadding:
            EdgeInsets.only(left: (leftpadding != null) ? leftpadding : 10),
        filled: true,
        fillColor:fillColor,
        prefixIcon: prefixIcon,
        suffixIcon: suffixIcon,
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular((borderRadius!=null)?borderRadius:10),
          borderSide: BorderSide(
            color: appColor.transparent,
          ),

        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular((borderRadius!=null)?borderRadius:10),
          borderSide: BorderSide(
            color: appColor.transparent,
          ),
        ),
      ),
    );
  }

  static Widget customButton({
    required VoidCallback onPressed,
    required String btnTitle,
    Color? color,
    Color? fontColor,
    Icon? icons,
    double? borderRadius,
    double? width,
    required BuildContext context,
    FontWeight? fontWeight,
  }) {
    return GestureDetector(
      onTap: onPressed,
      child: Container(
        height: 50,
        width: (width != null) ? width : MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
            color: (color != null) ? color : appColor.black,
            borderRadius: BorderRadius.circular(
                (borderRadius != null) ? borderRadius : 30)),
        child: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(margin: EdgeInsets.only(right: resize(10)), child: icons),
              Text(
                btnTitle,
                style: TextStyle(
                    fontSize: resize(20),
                    color: (fontColor != null) ? fontColor : appColor.white,
                    fontWeight: fontWeight),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
