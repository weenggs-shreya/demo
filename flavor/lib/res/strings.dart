Strings strings = Strings();

final title = "Strings";

class Strings {
  static final Strings _strings = Strings._i();

  factory Strings() {
    return _strings;
  }

  Strings._i();

  final String welcome_online_store = 'Welcome to our online store. One place for shopping, coupons and rewards!';
  final String welcome_to_grocery = 'Welcome to Grocery';
  final String welcome_back = 'Welcome back!';
  final String sign_in_continue = 'Sign In to continue';
  final String sign_up = 'Sign Up';
  final String sign_in = 'Sign In';
  final String login = 'Login';
  final String register = 'Register';
  final String new_user = 'New User';
  final String first_name = "First Name";
  final String last_name = "Last Name";
  final String zip_code = "Zip Code(5 digits Only)";
  final String phone_no = "Phone Number(10 digits only)";
  final String email_address = "Email Address";
  final String  submit= "Submit";
  final String  cancel= "Cancel";
  final String  password= "Password";
  final String forgot_password="Forgot Password";
  final String  confirm_password= "Confirm Password";
  final String  choose_language= "Choose Language";
  final String  by_tapping_submit= 'By Tapping :"SUBMIT"I agree';
  final String t_c_privacy = 'T&C and Privacy Policy';
  final String continue_ = 'Continue as guest';
  final String by_tapping_continue = 'By Tapping Continue as guest account, I agree to';
  final String already_have_not_an_account = 'Already have not an account?';
  final String grocery_shop_is_the_only_north_wales = 'Grocery shop is the only north wales based local focused recruiter.';
  final String this_will_be_your_username = "This will be your username";
  final String phone_number_is_used_as_your_loyalty = "(Phone number is used as your loyalty number)";

  final String error_internet = "Internet Connection Not Available!";
  final String select_preferred_store = "Select Preferred Store";
  final String is_required = " is required.";
  final String plz_enter_phone = " Please Enter your phone number";
}
