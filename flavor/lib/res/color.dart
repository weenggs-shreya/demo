import 'package:flutter/material.dart';

AppColor appColor = AppColor();

class AppColor {
  static final AppColor _appColor = AppColor._i();

  factory AppColor() {
    return _appColor;
  }

  AppColor._i();

  final Color white = Colors.white;
  final Color black = Colors.black;
  final Color transparent = Colors.transparent;
  final Color grey = Color(0xff707070);
  final Color lightgrey = Color(0xFFEEEEEE);
  final Color themeOrange = Color(0XFFEF4123);
  final Color themeblack = Color(0XFF191C1D);
  final Color blue = Color(0xFF2196F3);
  final Color darkblue = Color(0xFF304FFE);
  final Color notWhite = Color(0xFFEDF0F2);
  final Color red = Color(0xFFCA0832);
  final Color orange = Color(0xFFFF6D00);
  final Color orange1 = Color(0xFFff9973);
}
