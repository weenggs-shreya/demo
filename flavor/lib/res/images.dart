Images images = Images();

class Images {
  static final Images _images = Images._i();

  factory Images() {
    return _images;
  }

  Images._i();

  static const String _imageDir = "assets/images";

  final String ic_cardLanding = "$_imageDir/ic_cardLanding.png";
  final String ic_passwordLock = "$_imageDir/ic_passwordLock.png";
  final String ic_selectCheck = "$_imageDir/ic_selectCheck.png";
  final String ic_selectLanguage = "$_imageDir/ic_selectLanguage.png";
  final String superSave_logo = "$_imageDir/superSave_logo.png";
  final String grocery = "$_imageDir/grocery.png";
  final String lowes_logo = "$_imageDir/lowes_logo.png";
}
