class AppConstant {
  static const baseUrl_grocery =
      "https://lowessupersave.rsaamerica.com/Services/SSWebRestApi.svc";
  static const baseUrl_lowesMarket =
      "https://lowesmarket.rsaamerica.com/Services/SSWebRestApi.svc";
  static const Login_endPoint =
      "/ValidateUser";
  static const Register_endPoint =
      "/Register";

  static const  Get_ClientStore=
      "/GetClientStoresForApp/1";
}
