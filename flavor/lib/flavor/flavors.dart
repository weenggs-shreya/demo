import '../res/app_constant.dart';

enum Flavor {
  LOWES,
  GROCERY,
}

class F {
  static Flavor? appFlavor;

  static String get name => appFlavor?.name ?? '';

  static String get title {
    switch (appFlavor) {
      case Flavor.LOWES:
        return 'Lowes';
      case Flavor.GROCERY:
        return 'Grocery';
      default:
        return 'title';
    }
  }
  static String get Url {
    switch (appFlavor) {
      case Flavor.GROCERY:
        return AppConstant.baseUrl_grocery;
      case Flavor.LOWES:
        return AppConstant.baseUrl_lowesMarket;
      default:
        return 'title';
    }
  }
}
