import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:get/get.dart';
import 'package:fluttertoast/fluttertoast.dart';
import '../data/api/api_client.dart';
import '../data/common/constant.dart';
import '../data/model/login.dart';
import '../data/repository/api_repo.dart';
import '../flavor/flavors.dart';

class LoginController extends GetxController {
  RxBool textFieldEnabled = false.obs;

  RxBool isLoading = false.obs;
  RxBool passVisible = false.obs;

  ApiRepo apiRepo = ApiRepo();

  LoginDetailModel? _loginData;

  void showLoginPassword() {
    passVisible.value = !passVisible.value;
    update();
  }

  Future<void> signIn({required String email, password}) async {
    isLoading.value = true;
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.none) {
      constants.showSnackBar(ApiClient().getNetworkError());
    }
    try {
      final response = await apiRepo.signIn(email: email, password: password);
      isLoading.value = false;
      if (response.statusCode == 200) {
        _loginData = LoginDetailModel.fromJson(response.data);
        print("Login Detail:- " + "${_loginData?.Username}");
        if (_loginData?.ErrorMesasge?.ErrorCode == 1) {
          switch (F.appFlavor) {
            case Flavor.GROCERY:
              return print("Grocery " + F.title);
            case Flavor.LOWES:
              return print("Lowes " + F.title);
            default:
              return;
          }
          print("Successfully Login");
          // Get.to(() => WhiteFoodHomePage());
        } else {
          Fluttertoast.showToast(msg: "Invalid user", fontSize: 15);
        }
      }
    } catch (e) {
      print("Error....." + e.toString());
    }

    update();
  }

  Future<void> register({
    required String email,
    password,
    firstname,
    lastname,
    zipcode,
  }) async {
    isLoading.value = true;
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.none) {
      constants.showSnackBar(ApiClient().getNetworkError());
    }
    try {
      final response = await apiRepo.register(
        email: email,
        password: password,
        firstname: firstname,
        lastname: lastname,
        zipcode: zipcode,
      );
      isLoading.value = false;
      if (response.statusCode == 200) {
        _loginData = LoginDetailModel.fromJson(response.data);
        if (_loginData?.ErrorMessage?.ErrorCode == 1) {
          Get.back();
          Fluttertoast.showToast(msg: "Successfully Register");
        } else {
          Fluttertoast.showToast(msg: "user Already Exist", fontSize: 15);
        }
      }
    } catch (e) {
      print("Error....." + e.toString());
    }

    update();
  }
}
