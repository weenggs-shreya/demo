import 'package:grocery_app/res/strings.dart';

class Validate {
  static var emailPattern =
      r"^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$";
  static RegExp emailRegEx = RegExp(emailPattern);

  static bool isEmail(String value) {
    if (emailRegEx.hasMatch(value.trim())) {
      return true;
    }
    return false;
  }

  static String? validateEmail(String value) {
    String email = value.trim();
    if (email.isEmpty) {
      return 'Email is required.';
    }
    if (!isEmail(email)) {
      return 'Enter valid email address.';
    }
    return null;
  }

  static bool isValidEmail(String value) {
    var pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    return regex.hasMatch(value);
  }

  static bool isValidMobile(String value) {
    String patttern = r'(^(?:[+0]9)?[0-9]{6,15}$)';
    RegExp regExp = new RegExp(patttern);
    if (value.trim().isEmpty || value.length != 10 || !regExp.hasMatch(value)) {
      return false;
    }
    return true;
  }

  static String? validateMobile(String value) {
    String mobileno = value.trim();
    if (mobileno.isEmpty) {
      return 'Phone number is required.';
    }
    if (!isEmail(mobileno)) {
      return 'please Enter 10 digit number.';
    }
    return null;
  }

  static bool isValidZipCode(String value) {
    if (value.trim().isEmpty || value.length != 5) {
      return false;
    }
    return true;
  }

  static String? validateZipCode(String value) {
    String mobileno = value.trim();
    if (mobileno.isEmpty) {
      return 'Zip Code is required.';
    }
    if (!isEmail(mobileno)) {
      return 'please Enter your Zipcode in 5 digit';
    }
    return null;
  }

  static bool isValidPassword(String value, String value2) {
    if (value.trim().isEmpty || value != value2 || value2.trim().isEmpty) {
      return false;
    }
    return true;
  }

  static bool isrequired(String value) {
    if (value.trim().isEmpty) {
      return false;
    }
    return true;
  }

  static String requiredField(String title) {
    return title + strings.is_required;
  }
}
