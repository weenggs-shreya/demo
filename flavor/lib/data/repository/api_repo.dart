import 'package:dio/dio.dart';

import '../../flavor/flavors.dart';
import '../../res/app_constant.dart';
import '../api/api_client.dart';

final title = "ApiRepo";

class ApiRepo {
  ApiClient apiClient = ApiClient();

  Future<Response> signIn(
      {required String email, password}) async {
    Map<String, dynamic> params = {
      "MobileOS": "iOS",
      "UserName": email,
      "MobileOSVersion": "15.2",
      "DeviceRegistrationId": "2.11",
      "MobileModel": "",
      "CustomerId": 1,
      "Password": password,
      "DeviceInfo": "",
      "DeviceId": "2"
    };
    return await apiClient.post(
      url: F.Url + AppConstant.Login_endPoint,
      data: params,
      // queryParameters: params
    );
  }


  Future<Response> register(
      {required String email,required password,required lastname,required zipcode,required firstname}) async {
    Map<String, dynamic> params = {
      "LastName": lastname,
      "ZipCode":zipcode,
      "DeviceType": "",
      "DeviceId": "2",
      "FirstName": firstname,
      "UserName": email,
      "CustomerId": "1",
      "Password": password,
      "ClientStoreId": 1
    };
    return await apiClient.post(
      url: F.Url + AppConstant.Register_endPoint,
      data: params,
      // queryParameters: params
    );
  }

  Future<Response> GetClientStoreWhite() async {
    return await apiClient.get(
      url: AppConstant.baseUrl_lowesMarket + AppConstant.Get_ClientStore,
    );
  }
}
