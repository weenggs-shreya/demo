import 'dart:convert';

class ClientStoreModel {
  String? addressLine1;
  String? addressLine2;
  String? city;
  int? clientStoreId;
  String? clientStoreName;
  double? latitude;
  double? longitude;
  String? stateName;
  bool? storeEcomStatus;
  String? storeEmail;
  String? storeImage;
  int? storeNumber;
  String? storePhoneNumber;
  String? storeTimings;
  int? weeklyAdStoreId;
  String? zipCode;
  bool? ecomandroidstatus;
  bool? ecomiosstatus;
  String? ecomlabel;
  bool? mealkitandroidstatus;
  int? mealkitdeptnumber;
  bool? mealkitiosstatus;
  String? mealkitlabel;

  ClientStoreModel({
    this.addressLine1,
    this.addressLine2,
    this.city,
    this.clientStoreId,
    this.clientStoreName,
    this.latitude,
    this.longitude,
    this.stateName,
    this.storeEcomStatus,
    this.storeEmail,
    this.storeImage,
    this.storeNumber,
    this.storePhoneNumber,
    this.storeTimings,
    this.weeklyAdStoreId,
    this.zipCode,
    this.ecomandroidstatus,
    this.ecomiosstatus,
    this.ecomlabel,
    this.mealkitandroidstatus,
    this.mealkitdeptnumber,
    this.mealkitiosstatus,
    this.mealkitlabel,
  });

  factory ClientStoreModel.fromRawJson(String str) =>
      ClientStoreModel.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory ClientStoreModel.fromJson(Map<String, dynamic> json) =>
      ClientStoreModel(
        addressLine1: json["AddressLine1"] == null ? null : json["AddressLine1"],
        addressLine2: json["AddressLine2"] == null ? null : json["AddressLine2"],
        city: json["City"] == null ? null : json["City"],
        clientStoreId: json["ClientStoreId"] == null ? null : json["ClientStoreId"],
        clientStoreName:
            json["ClientStoreName"] == null ? null : json["ClientStoreName"],
        latitude: json["Latitude"] == null ? null : json["Latitude"],
        longitude: json["Longitude"] == null ? null : json["Longitude"],
        stateName: json["StateName"] == null ? null : json["StateName"],
        storeEcomStatus:
            json["StoreEcomStatus"] == null ? null : json["StoreEcomStatus"],
        storeEmail: json["StoreEmail"] == null ? null : json["StoreEmail"],
        storeImage: json["StoreImage"] == null ? null : json["StoreImage"],
        storeNumber: json["StoreNumber"] == null ? null : json["StoreNumber"],
        storePhoneNumber:
            json["StorePhoneNumber"] == null ? null : json["StorePhoneNumber"],
        storeTimings:
            json["StoreTimings"] == null ? null : json["StoreTimings"],
        weeklyAdStoreId:
            json["WeeklyAdStoreId"] == null ? null : json["WeeklyAdStoreId"],
        zipCode: json["ZipCode"] == null ? null : json["ZipCode"],
        ecomandroidstatus: json["ecomandroidstatus"] == null
            ? null
            : json["ecomandroidstatus"],
        ecomiosstatus:
            json["ecomiosstatus"] == null ? null : json["ecomiosstatus"],
        ecomlabel: json["ecomlabel"] == null ? null : json["ecomlabel"],
        mealkitandroidstatus: json["mealkitandroidstatus"] == null
            ? null
            : json["mealkitandroidstatus"],
        mealkitdeptnumber: json["mealkitdeptnumber"] == null
            ? null
            : json["mealkitdeptnumber"],
        mealkitiosstatus:
            json["mealkitiosstatus"] == null ? null : json["mealkitiosstatus"],
        mealkitlabel:
            json["mealkitlabel"] == null ? null : json["mealkitlabel"],
      );

  Map<String, dynamic> toJson() => {
        "AddressLine1": addressLine1 == null ? null : addressLine1,
        "AddressLine2": addressLine2 == null ? null : addressLine2,
        "City": city == null ? null : city,
        "ClientStoreId": clientStoreId == null ? null : clientStoreId,
        "ClientStoreName": clientStoreName == null ? null : clientStoreName,
        "Latitude": latitude == null ? null : latitude,
        "Longitude": longitude == null ? null : longitude,
        "StateName": stateName == null ? null : stateName,
        "StoreEcomStatus": storeEcomStatus == null ? null : storeEcomStatus,
        "StoreEmail": storeEmail == null ? null : storeEmail,
        "StoreImage": storeImage == null ? null : storeImage,
        "StoreNumber": storeNumber == null ? null : storeNumber,
        "StorePhoneNumber": storePhoneNumber == null ? null : storePhoneNumber,
        "StoreTimings": storeTimings == null ? null : storeTimings,
        "WeeklyAdStoreId": weeklyAdStoreId == null ? null : weeklyAdStoreId,
        "ZipCode": zipCode == null ? null : zipCode,
        "ecomandroidstatus": ecomandroidstatus == null ? null : ecomandroidstatus,
        "ecomiosstatus": ecomiosstatus == null ? null : ecomiosstatus,
        "ecomlabel": ecomlabel == null ? null : ecomlabel,
        "mealkitandroidstatus": mealkitandroidstatus == null ? null : mealkitandroidstatus,
        "mealkitdeptnumber": mealkitdeptnumber == null ? null : mealkitdeptnumber,
        "mealkitiosstatus": mealkitiosstatus == null ? null : mealkitiosstatus,
        "mealkitlabel": mealkitlabel == null ? null : mealkitlabel,
      };
}
