import 'dart:convert';

import 'error.dart';

class LoginDetailModel {
  final String? AOGApiEndPoint;
  final String? AndroidHomeImage;
  final String? AndroidVersion;
  final int? ClientStoreId;
  final String? ClientStoreName;
  final int? CustomerId;
  final String? EnterpriseId;
  final String? FirstName;
  final String? LastName;
  final String? MemberNumber;
  final int? MobileNumber;
  final String? MyCardBarCodeImagePath;
  final String? SecurityKey;
  final bool? ShowAddToCartForSpecials;
  final bool? ShowPriceInSpecials;
  final int? SpecialsDisplayFormat;
  final int? UserDetailId;
  final String? UserToken;
  final String? Username;
  final String? ZipCode;
  ErrorMessageModel? ErrorMesasge;
  ErrorMessageModel? ErrorMessage;

  LoginDetailModel({
    this.AOGApiEndPoint,
    this.AndroidHomeImage,
    this.AndroidVersion,
    this.ClientStoreId,
    this.ClientStoreName,
    this.CustomerId,
    this.EnterpriseId,
    this.FirstName,
    this.LastName,
    this.MemberNumber,
    this.MobileNumber,
    this.MyCardBarCodeImagePath,
    this.SecurityKey,
    this.ShowAddToCartForSpecials,
    this.ShowPriceInSpecials,
    this.SpecialsDisplayFormat,
    this.UserDetailId,
    this.UserToken,
    this.Username,
    this.ZipCode,
    this.ErrorMesasge,
    this.ErrorMessage
  });

  factory LoginDetailModel.fromRawJson(String str) =>
      LoginDetailModel.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory LoginDetailModel.fromJson(Map<String, dynamic> json) =>
      LoginDetailModel(
        AOGApiEndPoint:
            json["AOGApiEndPoint"] == null ? null : json["AOGApiEndPoint"],
        AndroidHomeImage:
            json["AndroidHomeImage"] == null ? null : json["AndroidHomeImage"],
        AndroidVersion:
            json["AndroidVersion"] == null ? null : json["AndroidVersion"],
        ClientStoreId:
            json["ClientStoreId"] == null ? null : json["ClientStoreId"],
        ClientStoreName:
            json["ClientStoreName"] == null ? null : json["ClientStoreName"],
        CustomerId: json["CustomerId"] == null ? null : json["CustomerId"],
        EnterpriseId:
            json["EnterpriseId"] == null ? null : json["EnterpriseId"],
        FirstName: json["FirstName"] == null ? null : json["FirstName"],
        LastName: json["LastName"] == null ? null : json["LastName"],
        MemberNumber:
            json["MemberNumber"] == null ? null : json["MemberNumber"],
        MobileNumber:
            json["MobileNumber"] == null ? null : json["MobileNumber"],
        MyCardBarCodeImagePath: json["MyCardBarCodeImagePath"] == null
            ? null
            : json["MyCardBarCodeImagePath"],
        SecurityKey: json["SecurityKey"] == null ? null : json["SecurityKey"],
        ShowAddToCartForSpecials: json["ShowAddToCartForSpecials"] == null
            ? null
            : json["ShowAddToCartForSpecials"],
        ShowPriceInSpecials: json["ShowPriceInSpecials"] == null
            ? null
            : json["ShowPriceInSpecials"],
        SpecialsDisplayFormat: json["SpecialsDisplayFormat"] == null
            ? null
            : json["SpecialsDisplayFormat"],
        UserDetailId:
            json["UserDetailId"] == null ? null : json["UserDetailId"],
        UserToken: json["UserDetailId"] == null ? null : json["UserToken"],
        Username: json["Username"] == null ? null : json["Username"],
        ZipCode: json["ZipCode"] == null ? null : json["ZipCode"],
        ErrorMesasge: json["ErrorMesasge"] == null
            ? null
            : ErrorMessageModel.fromJson(json["ErrorMesasge"]),
        ErrorMessage: json["ErrorMessage"] == null
            ? null
            : ErrorMessageModel.fromJson(json["ErrorMessage"]),
      );

  Map<String, dynamic> toJson() => {
        "AOGApiEndPoint": AOGApiEndPoint == null ? null : AOGApiEndPoint,
        "AndroidHomeImage": AndroidHomeImage == null ? null : AndroidHomeImage,
        "AndroidVersion": AndroidVersion == null ? null : AndroidVersion,
        "ClientStoreId": ClientStoreId == null ? null : ClientStoreId,
        "ClientStoreName": ClientStoreName == null ? null : ClientStoreName,
        "CustomerId": CustomerId == null ? null : CustomerId,
        "EnterpriseId": EnterpriseId == null ? null : EnterpriseId,
        "FirstName": FirstName == null ? null : FirstName,
        "LastName": LastName == null ? null : LastName,
        "MemberNumber": MemberNumber == null ? null : MemberNumber,
        "MobileNumber": MobileNumber == null ? null : MobileNumber,
        "MyCardBarCodeImagePath":
            MyCardBarCodeImagePath == null ? null : MyCardBarCodeImagePath,
        "SecurityKey": SecurityKey == null ? null : SecurityKey,
        "ShowAddToCartForSpecials":
            ShowAddToCartForSpecials == null ? null : ShowAddToCartForSpecials,
        "ShowPriceInSpecials":
            ShowPriceInSpecials == null ? null : ShowPriceInSpecials,
        "SpecialsDisplayFormat":
            SpecialsDisplayFormat == null ? null : SpecialsDisplayFormat,
        "UserDetailId": UserDetailId == null ? null : UserDetailId,
        "UserToken": UserToken == null ? null : UserToken,
        "Username": Username == null ? null : Username,
        "ZipCode": ZipCode == null ? null : ZipCode,
        "ErrorMesasge": ErrorMesasge == null ? null : ErrorMesasge,
        "ErrorMessage": ErrorMessage == null ? null : ErrorMessage,
      };
}
