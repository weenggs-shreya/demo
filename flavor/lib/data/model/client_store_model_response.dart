import 'ClientStoresModel.dart';
import 'error.dart';

class ClientStoresModel {
  List<ClientStoreModel>? clientStoresList;
  bool? enableStoreManagement;
  ErrorMessageModel? errorMessage;

  ClientStoresModel({
    this.clientStoresList,
    this.enableStoreManagement,
    this.errorMessage,
  });

  factory ClientStoresModel.fromJson(Map<String, dynamic> json) =>
      ClientStoresModel(
        clientStoresList: List<ClientStoreModel>.from(
            json["ClientStoresList"].map((x) => ClientStoreModel.fromJson(x))),
        errorMessage: json["ErrorMessage"] == null
            ? null
            : ErrorMessageModel.fromJson(json["ErrorMesasge"]),
        enableStoreManagement: json["EnableStoreManagement"] == null ? null : json["EnableStoreManagement"],
      );

// Map<String, dynamic> toJson() => {
//   "clientStoresList": List<ClientStoreModel>.from(clientStoresList.map((x) => x.toJson())),
// };
}
