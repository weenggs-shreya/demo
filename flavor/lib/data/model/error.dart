import 'dart:convert';

class ErrorMessageModel {
  int? ErrorCode;
  String? ErrorDetails;
  String? OfflineMessage;

  ErrorMessageModel({
    this.ErrorCode,
    this.ErrorDetails,
    this.OfflineMessage,
  });

  factory ErrorMessageModel.fromRawJson(String str) =>
      ErrorMessageModel.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory ErrorMessageModel.fromJson(Map<String, dynamic> json) =>
      ErrorMessageModel(
        ErrorCode: json["ErrorCode"] == null ? null : json["ErrorCode"],
        ErrorDetails: json["ErrorDetails"] == null ? null : json["ErrorDetails"],
        OfflineMessage: json["OfflineMessage"] == null ? null : json["OfflineMessage"],
      );

  Map<String, dynamic> toJson() => {
    "ErrorCode": ErrorCode == null ? null : ErrorCode,
    "ErrorDetails": ErrorDetails == null ? null : ErrorDetails,
    "OfflineMessage": OfflineMessage == null ? null : OfflineMessage,
  };
}
