import 'package:flutter/material.dart';
import 'package:grocery_app/data/common/validation.dart';
import 'package:grocery_app/screens/lowes/lowes_login_page.dart';

import '../../controller/login_controller.dart';
import '../../data/common/constant.dart';
import '../../flavor/flavors.dart';
import '../../res/color.dart';
import '../../res/strings.dart';
import '../../widget/custom_progree_view.dart';

import 'package:get/get.dart';

import 'package:fluttertoast/fluttertoast.dart';

import '../grocery/grocery_login_home_page.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final LoginController _controller = Get.put(LoginController());

  @override
  Widget build(BuildContext context) {
    switch (F.appFlavor) {
      case Flavor.GROCERY:
        return GroceryLoginHomePage(
          loginController: _controller,
          logincallback: (email, password) {
            _login(email, password);
          },
          registercallback: (fname, lname, zipcode, phoneNo, email, password,
              confirmPassword) {
            _signup(fname, lname, zipcode, phoneNo, email, password,
                confirmPassword);
          },
        );
      case Flavor.LOWES:
        return Obx((){
          return CustomProgressView(
            isLoading: _controller.isLoading.isTrue,
            indicatorColor: appColor.themeOrange,
            child: LowesLoginPage(
              loginController: _controller,
              logincallback: (email, password) {
                _login(email, password);
              },
              registercallback: (fname, lname, zipcode, phoneNo, email, password,
                  confirmPassword) {
                _signup(fname, lname, zipcode, phoneNo, email, password,
                    confirmPassword);
              },
            ),
          );
        });
      default:
        return Container();
    }
  }

  _login(String _email, String _password) {
    constants.dismissKeyboard(context);
    if (!Validate.isValidEmail(_email)) {
      Fluttertoast.showToast(
          msg: "${Validate.validateEmail(_email)}", fontSize: 15);
    } else {
      _controller.signIn(
        email: _email,
        password: _password,
      );
      _email = "";
      _password = "";
    }
  }

  void _signup(String fname, String lname, String zipcode, String phoneNo,
      String email, String password, String confirmPassword) {
    if (!Validate.isrequired(fname)) {
      Fluttertoast.showToast(
          msg: "${Validate.requiredField("first Name")}", fontSize: 15);
    } else if (!Validate.isrequired(lname)) {
      Fluttertoast.showToast(
          msg: "${Validate.requiredField("Last Name")}", fontSize: 15);
    } else if (!Validate.isValidZipCode(zipcode)) {
      Fluttertoast.showToast(
          msg: "${Validate.validateZipCode(zipcode)}", fontSize: 15);
    } else if (!Validate.isValidMobile(phoneNo)) {
      Fluttertoast.showToast(
          msg: "${Validate.validateMobile(phoneNo)}", fontSize: 15);
    } else if (!Validate.isValidEmail(email)) {
      Fluttertoast.showToast(
          msg: "${Validate.validateEmail(email)}", fontSize: 15);
    } else if (!Validate.isrequired(password)) {
      Fluttertoast.showToast(
          msg: "${Validate.requiredField("PassWord")}", fontSize: 15);
    } else if (!Validate.isrequired(confirmPassword)) {
      Fluttertoast.showToast(
          msg: "${Validate.requiredField("Confirm Password")}", fontSize: 15);
    } else if (!Validate.isValidPassword(password, confirmPassword)) {
      Fluttertoast.showToast(
          msg: "${Validate.requiredField("please Enter your same Password")}",
          fontSize: 15);
    } else {
      _controller.register(
        email: email,
        firstname: fname,
        password: confirmPassword,
        lastname: lname,
        zipcode: zipcode,
      );
      fname = "";
      lname = "";
      zipcode = "";
      phoneNo = "";
      email = "";
      password = "";
      confirmPassword = "";
    }
  }
}
