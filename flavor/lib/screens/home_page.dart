import 'package:flutter/material.dart';
import 'package:grocery_app/res/images.dart';
import 'package:grocery_app/screens/base/login.dart';


class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage(images.grocery),
            fit: BoxFit.cover,
          ),
        ),
        child: LoginPage(),
      ),
    );
  }
}
