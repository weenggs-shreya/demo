import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:grocery_app/res/images.dart';
import 'package:grocery_app/screens/lowes/lowes_register_page.dart';
import 'package:multiscreen/multiscreen.dart';

import '../../controller/login_controller.dart';
import '../../res/color.dart';
import '../../data/common/constant.dart';
import '../../res/strings.dart';
import '../../utils/widget_utils.dart';

class LowesLoginPage extends StatefulWidget {
  final LoginController loginController;
  final Function logincallback;
  final Function registercallback;

  LowesLoginPage({
    Key? key,
    required this.loginController,
    required this.logincallback,
    required this.registercallback,
  }) : super(key: key);

  @override
  State<LowesLoginPage> createState() => _LowesLoginPageState();
}

class _LowesLoginPageState extends State<LowesLoginPage> {
  bool value = false;
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  bool hidePassword = true;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        constants.dismissKeyboard(context);
      },
      child: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.all(20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  WidgetUtils.spaceVertical(30),
                  ElevatedButton(
                    onPressed: () {},
                    style: ElevatedButton.styleFrom(
                        primary: appColor.black,
                        side: BorderSide(
                          color: appColor.themeOrange,
                          width: 1,
                        ),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5))),
                    child: Text(strings.choose_language),
                  ),
                  Container(
                    margin:
                        EdgeInsets.only(top: resize(30), bottom: resize(20)),
                    child: Center(
                        child: Image.asset(
                      images.lowes_logo,
                      height: resize(100),
                    )),
                  ),
                  Text(
                    strings.welcome_online_store,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 20,
                      color: appColor.white,
                    ),
                  ),
                  WidgetUtils.spaceVertical(20),
                  WidgetUtils.buildTextField(
                    hintText: strings.email_address,
                    prefixIcon: Icon(Icons.person, color: appColor.grey),
                    controller: _emailController,
                  ),
                  WidgetUtils.spaceVertical(20),
                  WidgetUtils.buildTextField(
                    hintText: strings.password,
                    prefixIcon: Icon(Icons.lock, color: appColor.grey),
                    controller: _passwordController,
                    isVisible: hidePassword,
                    suffixIcon: IconButton(
                      icon: hidePassword
                          ? Icon(Icons.visibility_off)
                          : Icon(Icons.visibility),
                      onPressed: () {
                        setState(() {
                          hidePassword = !hidePassword;
                        });
                      },
                    ),
                  ),
                  WidgetUtils.spaceVertical(10),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          Checkbox(
                            materialTapTargetSize:
                                MaterialTapTargetSize.shrinkWrap,
                            value: this.value,
                            onChanged: (value) {
                              setState(() {
                                this.value = value!;
                              });
                            },
                          ),
                          Text(
                            "Remember Me",
                            style: TextStyle(
                                fontSize: resize(15), color: appColor.white),
                          ),
                        ],
                      ),
                      InkWell(
                        child: Text(
                          strings.forgot_password,
                          style: TextStyle(
                            fontSize: resize(15),
                            color: appColor.white,
                            // decoration: TextDecoration.underline,
                          ),
                        ),
                        onTap: () {},
                      ),
                    ],
                  ),
                  WidgetUtils.spaceVertical(30),
                  WidgetUtils.customButton(
                    context: context,
                    onPressed: () {
                      widget.logincallback(
                          _emailController.text, _passwordController.text);
                    },
                    btnTitle: strings.login,
                    color: appColor.orange,
                    icons: Icon(
                      Icons.login_rounded,
                      color: appColor.white,
                    ),
                  ),
                  WidgetUtils.spaceVertical(20),
                  WidgetUtils.customButton(
                    context: context,
                    onPressed: () {
                      Get.to(()=>LowesRegisterPage(submitCallback: widget.registercallback,));

                    },
                    btnTitle: strings.new_user,
                    color: appColor.black,
                    icons: Icon(
                      Icons.person,
                      color: appColor.white,
                    ),
                  ),
                  WidgetUtils.spaceVertical(100),
                ],
              ),
            ),
            Text(
              strings.continue_,
              style: TextStyle(
                  color: appColor.white,
                  fontSize: 20,
                  decoration: TextDecoration.underline),
            ),
            WidgetUtils.spaceVertical(20),
            Text(
              strings.by_tapping_continue,
              style: TextStyle(
                color: appColor.white,
                fontSize: 17,
              ),
            ),
            Text(
              strings.t_c_privacy,
              style: TextStyle(
                  color: appColor.blue,
                  fontSize: 17,
                  fontWeight: FontWeight.bold),
            )
          ],
        ),
      ),
    );
  }
}
