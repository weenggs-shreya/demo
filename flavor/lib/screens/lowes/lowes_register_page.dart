import 'package:flutter/material.dart';
import 'package:grocery_app/res/images.dart';
import 'package:multiscreen/multiscreen.dart';

import '../../res/color.dart';
import '../../data/common/constant.dart';
import '../../res/strings.dart';
import '../../utils/widget_utils.dart';

class LowesRegisterPage extends StatefulWidget {
  final Function submitCallback;
  final Function? cancelCallback;

  LowesRegisterPage({
    required this.submitCallback,
    this.cancelCallback,
  }) : super();

  @override
  State<LowesRegisterPage> createState() => _LowesRegisterPageState();
}

class _LowesRegisterPageState extends State<LowesRegisterPage> {
  TextEditingController _firstNameController = TextEditingController();
  TextEditingController _lastNameController = TextEditingController();
  TextEditingController _zipController = TextEditingController();
  TextEditingController _phoneController = TextEditingController();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  TextEditingController _confirmPasswordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          strings.sign_up.toUpperCase(),
          style: TextStyle(color: appColor.black),
        ),
        backgroundColor: appColor.notWhite,
        elevation: 0,
        iconTheme: IconThemeData(color: appColor.black),
      ),
      body: SingleChildScrollView(
        child: GestureDetector(
          onTap: () {
            constants.dismissKeyboard(context);
          },
          child: Container(
            padding: EdgeInsets.only(left: resize(20), right: resize(20)),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Container(
                    margin: EdgeInsets.only(top: 10),
                    child: ElevatedButton(
                      onPressed: () {},
                      style: ElevatedButton.styleFrom(
                          side:
                              BorderSide(color: appColor.themeOrange, width: 1),
                          primary: appColor.black,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(5))),
                      child: Text(strings.choose_language),
                    )),
                Container(
                  margin: EdgeInsets.only(top: resize(30), bottom: resize(20)),
                  child: Center(
                      child: Image.asset(
                    images.lowes_logo,
                    height: resize(100),
                  )),
                ),
                Row(
                  children: [
                    Expanded(
                      child: WidgetUtils.buildTextField(
                          hintText: strings.first_name,
                          controller: _firstNameController),
                    ),
                    WidgetUtils.spaceHorizontal(10),
                    Expanded(
                      child: WidgetUtils.buildTextField(
                          hintText: strings.last_name,
                          controller: _lastNameController),
                    ),
                  ],
                ),
                WidgetUtils.spaceVertical(10),
                WidgetUtils.buildTextField(
                    textInputType: TextInputType.number,
                    hintText: strings.zip_code,
                    controller: _zipController),
                WidgetUtils.spaceVertical(10),
                WidgetUtils.buildTextField(
                    textInputType: TextInputType.number,
                    hintText: strings.phone_no,
                    controller: _phoneController),
                Align(
                    alignment: Alignment.bottomLeft,
                    child: Text(
                      strings.phone_number_is_used_as_your_loyalty,
                      style: TextStyle(color: appColor.red, fontSize: 15),
                    )),
                WidgetUtils.spaceVertical(10),
                WidgetUtils.buildTextField(
                  hintText: strings.email_address,
                  textInputType: TextInputType.emailAddress,
                  controller: _emailController,
                  suffixIcon: IconButton(
                    onPressed: () {},
                    icon: Icon(
                      Icons.check_circle_rounded,
                      color: appColor.themeOrange,
                    ),
                  ),
                ),
                Align(
                    alignment: Alignment.bottomRight,
                    child: Text(
                      strings.this_will_be_your_username,
                      style: TextStyle(color: appColor.grey, fontSize: 15),
                    )),
                WidgetUtils.spaceVertical(10),
                Row(
                  children: [
                    Expanded(
                      child: WidgetUtils.buildTextField(
                          hintText: strings.password,
                          controller: _passwordController),
                    ),
                    WidgetUtils.spaceHorizontal(10),
                    Expanded(
                      child: WidgetUtils.buildTextField(
                          hintText: strings.confirm_password,
                          controller: _confirmPasswordController),
                    ),
                  ],
                ),
                WidgetUtils.spaceVertical(30),
                Align(
                  alignment: Alignment.topLeft,
                  child: RichText(
                    text: TextSpan(
                      text: strings.by_tapping_submit,
                      style: TextStyle(color: appColor.black),
                      children: <TextSpan>[
                        TextSpan(
                            text: strings.t_c_privacy,
                            style: TextStyle(color: appColor.darkblue)),
                      ],
                    ),
                  ),
                ),
                WidgetUtils.spaceVertical(10),
                WidgetUtils.customButton(
                  onPressed: () {
                    widget.submitCallback(
                      _firstNameController.text,
                      _lastNameController.text,
                      _zipController.text,
                      _phoneController.text,
                      _emailController.text,
                      _passwordController.text,
                      _confirmPasswordController.text,
                    );
                  },
                  btnTitle: strings.submit.toUpperCase(),
                  color: appColor.orange,
                  context: context,
                  borderRadius: 10,
                ),
                WidgetUtils.spaceVertical(15),
                WidgetUtils.customButton(
                  onPressed: () {
                    _firstNameController.text = "";
                    _lastNameController.text = "";
                    _zipController.text = "";
                    _confirmPasswordController.text = "";
                    _passwordController.text = "";
                    _emailController.text = "";
                  },
                  btnTitle: strings.cancel.toUpperCase(),
                  color: appColor.black,
                  context: context,
                  borderRadius: 10,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
