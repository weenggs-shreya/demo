import 'package:flutter/material.dart';
import 'package:grocery_app/res/color.dart';
import 'package:grocery_app/screens/grocery/grocery_login_page.dart';
import 'package:grocery_app/screens/grocery/grocery_register_page.dart';
import 'package:multiscreen/multiscreen.dart';

import '../../controller/login_controller.dart';
import '../../data/common/constant.dart';
import '../../res/images.dart';
import '../../res/strings.dart';
import '../../utils/widget_utils.dart';

class GroceryLoginHomePage extends StatefulWidget {
  final LoginController loginController;
  final Function logincallback;
  final Function registercallback;

  const GroceryLoginHomePage({
    required this.loginController,
    required this.logincallback,
    required this.registercallback,
  });

  @override
  State<GroceryLoginHomePage> createState() => _GroceryLoginHomePageState();
}

class _GroceryLoginHomePageState extends State<GroceryLoginHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: GestureDetector(
        onTap: () {
          constants.dismissKeyboard(context);
        },
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            WidgetUtils.spaceVertical(30),
            Container(
              margin: EdgeInsets.symmetric(vertical: 40),
              child: Image.asset(
                images.ic_cardLanding,
              ),
            ),
            Expanded(
              child: Container(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(40),
                      topRight: Radius.circular(40)),
                  gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [appColor.orange1, appColor.themeOrange],
                  ),
                ),
                child: Container(
                  margin: EdgeInsets.all(20),
                  child: Column(
                    children: [
                      WidgetUtils.spaceVertical(20),
                      Text(
                        strings.welcome_to_grocery,
                        style: TextStyle(
                            fontSize: 35,
                            fontWeight: FontWeight.bold,
                            color: appColor.white),
                      ),
                      WidgetUtils.spaceVertical(25),
                      Text(
                        strings.grocery_shop_is_the_only_north_wales,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 20,
                          color: appColor.white,
                        ),
                      ),
                      WidgetUtils.spaceVertical(40),
                      WidgetUtils.customButton(
                          context: context,
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => GroceryLoginPage(
                                          registercallback:
                                              widget.registercallback,
                                          logincallback: widget.logincallback,
                                          loginController:
                                              widget.loginController,
                                        )));
                          },
                          btnTitle: strings.login,
                          color: appColor.orange1,
                          fontWeight: FontWeight.bold),
                      WidgetUtils.spaceVertical(20),
                      WidgetUtils.customButton(
                        context: context,
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => GroceryRegisterPage(
                                        signupCallback: widget.registercallback,

                                      )));
                        },
                        btnTitle: strings.register,
                        color: appColor.white,
                        fontWeight: FontWeight.bold,
                        fontColor: appColor.orange1,
                      ),
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
