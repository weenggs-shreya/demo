import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:grocery_app/res/images.dart';
import 'package:grocery_app/utils/widget_utils.dart';
import 'package:grocery_app/widget/custom_progree_view.dart';

import '../../controller/login_controller.dart';
import '../../res/color.dart';
import '../../data/common/constant.dart';
import '../../res/strings.dart';

class GroceryLoginPage extends StatefulWidget {
  final LoginController? loginController;
  final Function? logincallback;
  final Function? registercallback;

  GroceryLoginPage({
     this.loginController,
     this.logincallback,
     this.registercallback,
  });

  @override
  State<GroceryLoginPage> createState() => _GroceryLoginPageState();
}

class _GroceryLoginPageState extends State<GroceryLoginPage> {
  bool value = false;
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Obx((){return  CustomProgressView(
      isLoading: widget.loginController!.isLoading.isTrue,
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            strings.sign_in.toUpperCase(),
            style: TextStyle(color: appColor.black),
          ),
          backgroundColor: appColor.notWhite,
          elevation: 0,
          iconTheme: IconThemeData(color: appColor.black),
          actions: [
            IconButton(
                onPressed: () {}, icon: Image.asset(images.ic_selectLanguage))
          ],
        ),
        body: GestureDetector(
          onTap: () {
            constants.dismissKeyboard(context);
          },
          child: SingleChildScrollView(
            child: Container(
              margin: EdgeInsets.all(15),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  WidgetUtils.spaceVertical(50),
                  Text(
                    strings.welcome_back,
                    style: TextStyle(
                        color: appColor.themeOrange,
                        fontSize: 40,
                        fontWeight: FontWeight.bold),
                  ),
                  Text(
                    strings.sign_in_continue,
                    style: TextStyle(
                      color: appColor.black,
                      fontSize: 20,
                    ),
                  ),
                  WidgetUtils.spaceVertical(50),
                  WidgetUtils.buildTextField(
                    hintText: strings.email_address,
                    prefixIcon: Icon(Icons.email, color: appColor.themeOrange),
                    controller: _emailController,
                    borderRadius: 50,
                    fillColor: appColor.grey.withOpacity(0.2),
                  ),
                  WidgetUtils.spaceVertical(20),
                  WidgetUtils.buildTextField(
                    hintText: strings.password,
                    prefixIcon: Icon(Icons.lock, color: appColor.themeOrange),
                    controller: _passwordController,
                    fillColor: appColor.grey.withOpacity(0.2),
                    borderRadius: 50,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          Checkbox(
                            materialTapTargetSize:
                            MaterialTapTargetSize.shrinkWrap,
                            value: this.value,
                            onChanged: (value) {
                              setState(() {
                                this.value = value!;
                              });
                            },
                          ),
                          Text(
                            "Remember Me",
                            style: TextStyle(fontSize: 20, color: appColor.black),
                          ),
                        ],
                      ),
                      InkWell(
                        child: Text(
                          strings.forgot_password,
                          style: TextStyle(
                            fontSize: 20,
                            color: appColor.themeOrange,
                            // decoration: TextDecoration.underline,
                          ),
                        ),
                        onTap: () {},
                      ),
                    ],
                  ),
                  WidgetUtils.spaceVertical(20),
                  WidgetUtils.customButton(
                    context: context,
                    onPressed: () {
                      widget.logincallback!(
                          _emailController.text, _passwordController.text);
                    },
                    btnTitle: strings.login,
                    color: appColor.themeOrange,
                    fontWeight: FontWeight.bold,
                  ),
                  WidgetUtils.spaceVertical(20),
                  Align(
                    alignment: Alignment.center,
                    child: Text(
                      strings.continue_,
                      style: TextStyle(
                          fontSize: 20, decoration: TextDecoration.underline),
                    ),
                  ),
                  WidgetUtils.spaceVertical(200),
                  Align(
                    alignment: Alignment.center,
                    child: RichText(
                      text: TextSpan(
                        text: strings.already_have_not_an_account,
                        style: TextStyle(color: appColor.black, fontSize: 15),
                        children: <TextSpan>[
                          TextSpan(
                              text: strings.sign_up,
                              style: TextStyle(color: appColor.themeOrange)),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );});

  }
}
