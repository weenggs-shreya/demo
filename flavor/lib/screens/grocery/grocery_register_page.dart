import 'package:flutter/material.dart';
import 'package:multiscreen/multiscreen.dart';

import '../../res/color.dart';
import '../../data/common/constant.dart';
import '../../res/strings.dart';
import '../../utils/widget_utils.dart';
import 'grocery_login_page.dart';

class GroceryRegisterPage extends StatefulWidget {
  Function signupCallback;

  GroceryRegisterPage({required this.signupCallback});

  @override
  State<GroceryRegisterPage> createState() => _GroceryRegisterPageState();
}

class _GroceryRegisterPageState extends State<GroceryRegisterPage> {
  TextEditingController _firstNameController = TextEditingController();
  TextEditingController _lastNameController = TextEditingController();
  TextEditingController _zipController = TextEditingController();
  TextEditingController _phoneController = TextEditingController();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  TextEditingController _confirmPasswordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: appColor.lightgrey,
      appBar: AppBar(
        title: Text(
          strings.sign_up.toUpperCase(),
          style: TextStyle(color: appColor.black),
        ),
        backgroundColor: appColor.notWhite,
        elevation: 0,
        iconTheme: IconThemeData(color: appColor.black),
      ),
      body: SingleChildScrollView(
        child: GestureDetector(
          onTap: () {
            constants.dismissKeyboard(context);
          },
          child: Container(
            margin: EdgeInsets.only(left: resize(5), right: resize(5)),
            color: appColor.white,
            height: MediaQuery.of(context).size.height,
            child: Container(
              padding: EdgeInsets.only(left: resize(10), right: resize(10)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  WidgetUtils.spaceVertical(30),
                  WidgetUtils.buildTextField(
                    hintText: strings.first_name,
                    fillColor: appColor.lightgrey,
                    borderRadius: 40,
                    controller: _firstNameController,
                  ),
                  WidgetUtils.spaceVertical(10),
                  WidgetUtils.buildTextField(
                      hintText: strings.last_name,
                      fillColor: appColor.lightgrey,
                      borderRadius: 40,
                      controller: _lastNameController),
                  WidgetUtils.spaceVertical(10),
                  WidgetUtils.buildTextField(
                      textInputType: TextInputType.number,
                      fillColor: appColor.lightgrey,
                      borderRadius: 40,
                      hintText: strings.zip_code,
                      controller: _zipController),
                  WidgetUtils.spaceVertical(10),
                  WidgetUtils.buildTextField(
                      textInputType: TextInputType.number,
                      fillColor: appColor.lightgrey,
                      borderRadius: 40,
                      hintText: strings.phone_no,
                      controller: _phoneController),
                  Align(
                      alignment: Alignment.bottomLeft,
                      child: Text(
                        strings.phone_number_is_used_as_your_loyalty,
                        style: TextStyle(color: appColor.black, fontSize: 15),
                      )),
                  WidgetUtils.spaceVertical(10),
                  WidgetUtils.buildTextField(
                    hintText: strings.email_address,
                    fillColor: appColor.lightgrey,
                    borderRadius: 40,
                    textInputType: TextInputType.emailAddress,
                    controller: _emailController,
                  ),
                  Align(
                      alignment: Alignment.bottomRight,
                      child: Text(
                        strings.this_will_be_your_username,
                        style: TextStyle(color: appColor.black, fontSize: 15),
                      )),
                  WidgetUtils.spaceVertical(10),
                  WidgetUtils.buildTextField(
                      hintText: strings.password,
                      fillColor: appColor.lightgrey,
                      borderRadius: 40,
                      controller: _passwordController),
                  WidgetUtils.spaceVertical(10),
                  WidgetUtils.buildTextField(
                      hintText: strings.confirm_password,
                      borderRadius: 40,
                      fillColor: appColor.lightgrey,
                      controller: _confirmPasswordController),
                  WidgetUtils.spaceVertical(30),
                  Align(
                    alignment: Alignment.topLeft,
                    child: RichText(
                      text: TextSpan(
                        text: strings.by_tapping_submit,
                        style: TextStyle(color: appColor.black),
                        children: <TextSpan>[
                          TextSpan(
                              text: strings.t_c_privacy,
                              style: TextStyle(color: appColor.darkblue)),
                        ],
                      ),
                    ),
                  ),
                  WidgetUtils.spaceVertical(30),
                  WidgetUtils.customButton(
                      onPressed: () {
                        widget.signupCallback(
                          _firstNameController.text,
                          _lastNameController.text,
                          _zipController.text,
                          _phoneController.text,
                          _emailController.text,
                          _passwordController.text,
                          _confirmPasswordController.text,
                        );
                      },
                      btnTitle: strings.sign_up.toUpperCase(),
                      color: appColor.themeOrange,
                      context: context,
                      borderRadius: 50,
                      fontWeight: FontWeight.bold),
                  WidgetUtils.spaceVertical(15),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(strings.already_have_not_an_account),
                      GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => GroceryLoginPage()));
                        },
                        child: Text(
                          strings.sign_in,
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.bold,
                              color: Colors.redAccent),
                        ),
                      )
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
