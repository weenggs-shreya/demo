import 'package:flutter/material.dart';
import 'app.dart';
import 'flavor/flavors.dart';


void main() {
  F.appFlavor = Flavor.GROCERY;
  runApp(App());
}
