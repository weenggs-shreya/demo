import 'dart:async';
import 'dart:math';
import 'package:flutter/material.dart';
import 'package:snake_game/snake_pixel.dart';
import 'blank_pixel.dart';
import 'button_widget.dart';
import 'food_pixel.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

enum snake_Direction { UP, DOWN, LEFT, RIGHT }

class _HomePageState extends State<HomePage> {
  //grid dimenation
  int rowSize = 20;
  int totalNumberOfSquares = 520;
  int currentScore = 0;
  bool gameHasStarted = false;
  late Timer timer;
  var colorArr = [
    0xffd61745,
    0xff3569bc,
    0xffffffff,
    0xfff4862a,
    0xffeaed19,
    0xff329f64,
    0xff000000
  ];

//Snake  Position
  List<int> snakePos = [0, 1, 2];

  //food Position
  int foodPos = 55;

//SNAKE Direction Right
  var currentDirection = snake_Direction.RIGHT;

  void startGame() {
    gameHasStarted = true;
    timer = Timer.periodic(Duration(milliseconds: 300), (timer) {
      setState(() {
        //moving snake
        moveSnake();
        //game over
        if (gameOver()) {
          timer.cancel();
          showDialog(
              context: context,
              barrierDismissible: false,
              builder: (context) {
                return AlertDialog(
                  title: Text("Game Over"),
                  actions: [
                    MaterialButton(
                        child: Text("Submit"),
                        onPressed: () {
                          Navigator.of(context).pop();
                          newGame();
                        })
                  ],
                );
              });
        }
      });
    });
  }

  void eatfood() {
    //new food is not where the snake is
    currentScore++;
    while (snakePos.contains(foodPos)) {
      foodPos = Random().nextInt(totalNumberOfSquares);
    }
  }

  bool gameOver() {
    //body of the snake
    List<int> bodySnake = snakePos.sublist(0, snakePos.length - 1);
    if (bodySnake.contains(snakePos.last)) {
      return true;
    }
    return false;
  }

  void moveSnake() {
    switch (currentDirection) {
      case snake_Direction.RIGHT:
        {
          //add new head
          //if snake is at the right wall,need to re-adjust
          if (snakePos.last % rowSize == rowSize - 1) {
            snakePos.add((snakePos.last + 1 - rowSize));
          } else {
            snakePos.add(snakePos.last + 1);
          }
        }
        break;
      case snake_Direction.LEFT:
        {
          //add new head
          //if snake is at the right wall,need to re-adjust
          if (snakePos.last % rowSize == 0) {
            snakePos.add((snakePos.last - 1 + rowSize));
          } else {
            snakePos.add(snakePos.last - 1);
          }
        }
        break;
      case snake_Direction.UP:
        {
          {
            //add new head
            if (snakePos.last < rowSize) {
              snakePos.add(snakePos.last - rowSize + totalNumberOfSquares);
            } else {
              snakePos.add(snakePos.last - rowSize);
            }
          }
        }
        break;
      case snake_Direction.DOWN:
        {
          {
            //add new head
            if (snakePos.last + rowSize >= totalNumberOfSquares) {
              snakePos.add(snakePos.last + rowSize - totalNumberOfSquares);
            } else {
              snakePos.add(snakePos.last + rowSize);
            }
          }
        }
        break;
      default:
    }
    //snake is eating food
    if (snakePos.last == foodPos) {
      eatfood();
    } else {
      //remove the tail
      snakePos.removeAt(0);
    }
  }

  void newGame() {
    setState(() {
      snakePos = [0, 1, 2];
      foodPos == 55;
      currentDirection = snake_Direction.RIGHT;
      gameHasStarted = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    // print("width "+MediaQuery.of(context).size.width.toString());
    // print(MediaQuery.of(context).size.height.toString());
    var size = MediaQuery.of(context).size;

    final double topPadding = size.height * 0.07;
    final double bottomPadding = size.height * 0.04;
    final double containerHeight = size.height - topPadding - bottomPadding;
    print("height :  " + size.height.toString());
    print("width : " + size.width.toString());
    print("containerHeight : " + containerHeight.toString());
    print("topPadding : " + topPadding.toString());
    print("bottomPadding : " + bottomPadding.toString());
    print("snakePos.first : " + snakePos.first.toString());

    rowSize = (size.width * 20 / 375).toInt();
    print("rowSize : " + rowSize.toString());
    totalNumberOfSquares =
        (((containerHeight * 0.7 * 26) / 505) * rowSize).toInt();
    if (totalNumberOfSquares % rowSize != 0) {
      int extra = totalNumberOfSquares % rowSize;
      totalNumberOfSquares = totalNumberOfSquares - extra;
    }
    // totalNumberOfSquares=4*20;
    print("totalNumberOfSquares : " + totalNumberOfSquares.toString());
    return Scaffold(
        // backgroundColor: Colors.grey.withOpacity(0.99),
        body: Container(
      height: size.height,
      child: Padding(
        padding: EdgeInsets.only(top: topPadding, bottom: bottomPadding),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            //game grid
            Container(
              alignment: Alignment.center,
              height: containerHeight * 0.7,
              child: Container(
                color: Colors.grey[900],
                child: GridView.builder(
                    shrinkWrap: true,
                    padding: EdgeInsets.all(0),
                    itemCount: totalNumberOfSquares,
                    physics: NeverScrollableScrollPhysics(),
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: rowSize,
                      crossAxisSpacing: 0,
                      mainAxisSpacing: 0,
                    ),
                    itemBuilder: (context, index) {
                      if (snakePos.contains(index)) {
                        return SnakePixel(
                          firstIndex: (snakePos.first == index),
                          lastIndex: snakePos.last == index,
                        );
                      } else if (foodPos == index) {
                        return FoodPixel();
                      } else {
                        return BlankPixel();
                      }
                    }),
              ),
            ),
            // Play Button

            Container(
              // color: Colors.red,
              height: containerHeight * 0.3,
              alignment: Alignment.center,
              child: ButtonWidget(
                upArrowCallback: () {
                  currentDirection = snake_Direction.UP;
                },
                downArrowCallback: () {
                  currentDirection = snake_Direction.DOWN;
                },
                leftArrowCallback: () {
                  currentDirection = snake_Direction.LEFT;
                },
                rightArrowCallback: () {
                  currentDirection = snake_Direction.RIGHT;
                },
                playCallback: () {
                  gameHasStarted ? onclickButton() : startGame();
                },
                play: gameHasStarted,
              ),
            ),
          ],
        ),
      ),
    ));
  }

  void onclickButton() {
    timer.cancel();
    gameHasStarted = false;
    setState(() {});
  }
}
