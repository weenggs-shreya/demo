import 'package:flutter/material.dart';

class ButtonWidget extends StatefulWidget {
  VoidCallback upArrowCallback;
  VoidCallback downArrowCallback;
  VoidCallback leftArrowCallback;
  VoidCallback rightArrowCallback;
  VoidCallback playCallback;
bool play;
  ButtonWidget({
    Key? key,
    required this.upArrowCallback,
    required this.downArrowCallback,
    required this.leftArrowCallback,
    required this.rightArrowCallback,
    required this.playCallback,
     this.play=false,
  }) : super(key: key);

  @override
  State<ButtonWidget> createState() => _ButtonWidgetState();
}

class _ButtonWidgetState extends State<ButtonWidget> {
  @override
  Widget build(BuildContext context) {
    return Stack(

      children: [
        Container(
          width: 150.0,
          height: 150.0,
          decoration: new BoxDecoration(
            color: Colors.black,
            shape: BoxShape.circle,
          ),
        ),
        Positioned(
          child: CircleButton(
              onTap: widget.upArrowCallback,
              iconData: Icons.keyboard_arrow_up_rounded),
          top: 5.0,
          left: 55.0,
        ),
        Positioned(
          child: CircleButton(
            onTap: widget.leftArrowCallback,
            iconData: Icons.keyboard_arrow_left_rounded,
          ),
          top: 55.0,
          left: 5.0,
        ),
        Positioned(
          child: CircleButton(
            onTap: widget.rightArrowCallback,
            iconData: Icons.keyboard_arrow_right_rounded,
          ),
          top: 55.0,
          right: 5.0,
        ),
        Positioned(
          child: CircleButton(
            onTap: widget.downArrowCallback,
            iconData: Icons.keyboard_arrow_down_rounded,
          ),
          top: 104.0,
          left: 55.0,
        ),
        Positioned(
          child: CircleButton(
            onTap: widget.playCallback,
            iconData:(!widget.play)? Icons.play_arrow:Icons.pause,
          ),
          top: 55.0,
          left: 55.0,
        ),
      ],
    );
  }
}

class CircleButton extends StatelessWidget {
  final GestureTapCallback onTap;
  final IconData iconData;

  const CircleButton({Key? key, required this.onTap, required this.iconData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    double size = 40.0;

    return InkResponse(
      onTap: onTap,
      child: Container(
        width: size,
        height: size,
        decoration: new BoxDecoration(
          color: Colors.white,
          shape: BoxShape.circle,
        ),
        child: Icon(
          iconData,
          color: Colors.black,
        ),
      ),
    );
  }
}
