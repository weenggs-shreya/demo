import 'package:flutter/material.dart';
import 'dart:math';

class SnakePixel extends StatelessWidget {
  bool firstIndex;
  bool lastIndex;

  SnakePixel({Key? key, this.firstIndex = false, this.lastIndex = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(0),
      child: getSnack(),
    );
  }

  Widget getSnack() {
    if (lastIndex) return getSnackFace();
    if (firstIndex) return getSnackTail();
    return Container(
      decoration: BoxDecoration(
        // color: Colors.primaries[Random().nextInt(Colors.primaries.length)],
        color: Colors.white,
      ),
    );
  }

  Widget getSnackFace() {
    return Container(
      decoration: BoxDecoration(
        // color: Colors.primaries[Random().nextInt(Colors.primaries.length)],
        color: Colors.red,
        // borderRadius: BorderRadius.only(
        //   bottomRight: Radius.circular(5),
        //   bottomLeft: Radius.circular(5),
        // ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Container(
            height: 5,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50),
              color: Colors.white,
            ),
            child: Text(
              "1",
              style: TextStyle(color: Colors.white),
            ),
          ),
          Container(
            height: 5,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50),
              color: Colors.white,
            ),
            child: Text(
              "1",
              style: TextStyle(color: Colors.white),
            ),
          ),
        ],
      ),
    );
  }

  Widget getSnackTail() {
    return Container(
      decoration: BoxDecoration(
        // color: Colors.primaries[Random().nextInt(Colors.primaries.length)],
        color: Colors.blue,
        borderRadius: BorderRadius.only(),
      ),
    );
  }
}
