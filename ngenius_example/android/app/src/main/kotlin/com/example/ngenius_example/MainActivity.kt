package com.example.ngenius_example

import android.app.Activity
import android.content.Intent
import android.util.Log
import com.google.gson.Gson
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import org.json.JSONObject
import payment.sdk.android.cardpayment.CardPaymentActivity
import payment.sdk.android.cardpayment.CardPaymentData
import payment.sdk.android.cardpayment.CardPaymentData.IntentResolver.STATUS_GENERIC_ERROR
import payment.sdk.android.cardpayment.CardPaymentData.IntentResolver.STATUS_PAYMENT_AUTHORIZED
import payment.sdk.android.cardpayment.CardPaymentData.IntentResolver.STATUS_PAYMENT_CAPTURED
import payment.sdk.android.cardpayment.CardPaymentData.IntentResolver.STATUS_PAYMENT_FAILED


class MainActivity : FlutterActivity() {
    private val CHANNEL = "com.example.ngenius_example";

    private lateinit var channel: MethodChannel
var result : MethodChannel.Result?=null;
    override fun configureFlutterEngine(flutterEngine: FlutterEngine) {
        super.configureFlutterEngine(flutterEngine)

        channel = MethodChannel(flutterEngine.dartExecutor.binaryMessenger, CHANNEL)

        channel.setMethodCallHandler() { call, result ->
            when {
                call.method.equals("orderResponse") -> {
                    orderResponse(call, result)
                }
            }

        }
    }

    private fun orderResponse(call: MethodCall, result: MethodChannel.Result) {
        var response = call.argument<String>("response");
        Log.d("tag", "order Response Android :- $response");
        this.result=result;
        try {
            var json: JSONObject = JSONObject(response!!);
            val url = json.getJSONObject("_links").getJSONObject("payment-authorization").getString("href");
            val code = json.getJSONObject("_links").getJSONObject("payment").getString("href").split("=")[1];
            val orderReference = json.getString("reference")
            startActivityForResult(CardPaymentActivity.Companion.getIntent(activity, url, code), 101);
        } catch (e: Exception) {
            e.printStackTrace()
        }

     //    result.success(response);
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 101) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    onCardPaymentResponse(CardPaymentData.getFromIntent(data!!))
                }
                Activity.RESULT_CANCELED -> {
                    result!!.success("RESULT_CANCELED");
                }
            }
        }
    }

    private fun onCardPaymentResponse(data: CardPaymentData) {
        when (data.code) {
            STATUS_PAYMENT_AUTHORIZED, STATUS_PAYMENT_CAPTURED -> {
               result!!.success("success - ${Gson().toJson(data)}")
            }
            STATUS_PAYMENT_FAILED -> {
               result!!.success("STATUS_PAYMENT_FAILED- ${Gson().toJson(data)}");
            }
            STATUS_GENERIC_ERROR -> {
                result!!.success("STATUS_GENERIC_ERROR- ${Gson().toJson(data)}");
            }
        }
    }

}