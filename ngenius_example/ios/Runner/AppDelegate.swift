import UIKit
import Flutter
import PassKit

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
    
    var result:FlutterResult?
    var paymentRequest : PKPaymentRequest?
  override func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
  ) -> Bool {
      
      GeneratedPluginRegistrant.register(with: self)
      let METHOD_CHANNEL_NAME = "com.example.ngenius_example"
      let controller:FlutterViewController=window?.rootViewController as! FlutterViewController
     
      let channel = FlutterMethodChannel(name:METHOD_CHANNEL_NAME,
                                                binaryMessenger: controller.binaryMessenger)
      channel.setMethodCallHandler({
          [weak self](call:FlutterMethodCall,result: @escaping FlutterResult)-> Void in
          if(call.method == "orderResponse"){
              self?.orderResponse(result: result, call: call)
          }
      })

    return super.application(application, didFinishLaunchingWithOptions: launchOptions)
  }
    
    private  func orderResponse(result: FlutterResult?,call: FlutterMethodCall) {
        self.result = result
        let obj : NSDictionary =  call.arguments as? NSDictionary ?? [:]
        let response = obj.value(forKey: "response") as? String ?? "";
        do {
            if let jsonResponse = call.arguments as? [String : Any]
            {

                let jsonData = Data(response.utf8)
                let dataModel = try! JSONDecoder().decode(OrderResponse.self, from: jsonData)
                
                
                self.isApplePayPaymentMethod()
                
                let sharedSDKInstance = NISdk.sharedInstance
                
                let languageCode =   "en"
                let lanCode = languageCode == "ar" ? languageCode : "en"
                
                
                sharedSDKInstance.setSDKLanguage(language: lanCode)
                DispatchQueue.main.async {

                    sharedSDKInstance.showCardPaymentViewWith(cardPaymentDelegate: self, overParent: self.window?.rootViewController ?? UIViewController(), for: dataModel)

                }

            }
        } catch  {
            print (error.localizedDescription)
        }
       
        print("********"+response);
     
    }
    
    func isApplePayPaymentMethod() {
        let amount = 100.00
            let productsAmount = 100.0
            
//            if paymentMethod == .ApplePay {
                let merchantId = "merchant.com.sahem.ae"
    //            assert(!merchantId.isEmpty, "You need to add your apple pay merchant ID above")
                paymentRequest = PKPaymentRequest()
                paymentRequest?.merchantIdentifier = merchantId
                paymentRequest?.countryCode = "AE"
                paymentRequest?.currencyCode = "AED"
                //paymentRequest?.requiredShippingContactFields = [.postalAddress, .emailAddress, .phoneNumber]
                paymentRequest?.merchantCapabilities = [.capabilityDebit, .capabilityCredit, .capability3DS]
                //paymentRequest?.requiredBillingContactFields = [.postalAddress, .name]
                paymentRequest?.paymentSummaryItems = [PKPaymentSummaryItem(label: "Item", amount: NSDecimalNumber(value: productsAmount)),PKPaymentSummaryItem(label:"Weenggs Technology", amount: NSDecimalNumber(value: amount))]

        }
    

}

extension AppDelegate: CardPaymentDelegate{
    func paymentDidComplete(with status: PaymentStatus) {
        if(status == .PaymentSuccess) {
            self.result!("Payment sccess")
           print("Payment sccess")

        } else if(status == .PaymentFailed) {
            self.result!("Payment failed")
            print("Payment failed")
        } else if(status == .PaymentCancelled) {
            self.result!("Payment cancel")
            print("Payment cancel")
         
        }
    }

    func authorizationDidComplete(with status: AuthorizationStatus) {
        if(status == .AuthFailed) {
          // Authentication failed
          return
        }
        // Authentication was successful
      }

}
