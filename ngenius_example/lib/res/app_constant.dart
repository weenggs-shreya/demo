class AppConstant {
  static const baseUrl =
      "https://api-gateway.sandbox.ngenius-payments.com/identity/auth/access-token";
  static const baseUrlCreateOrder =
      "https://api-gateway.sandbox.ngenius-payments.com/transactions/outlets/95b342c8-a447-4be2-8f74-614c7dc8b4a6/orders";
}
