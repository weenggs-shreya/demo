Strings strings = Strings();

final title = "Strings";

class Strings {
  static final Strings _strings = Strings._i();

  factory Strings() {
    return _strings;
  }

  Strings._i();

  final String error_internet = "Internet Connection Not Available!";
  final String  payment_gateway = 'Payment Gateway';


}
