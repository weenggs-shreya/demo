import 'package:get/get.dart';
import 'package:flutter/material.dart';


Constants constants = Constants();

final title = "Constants";

class Constants {
  static final Constants _constants = Constants._i();

  factory Constants() {
    return _constants;
  }

  Constants._i();

  String fcmToken = "";

  String capitalizeFirstLetter(String s) {
    return (s).length < 1
        ? ''
        : s[0].toUpperCase() + s.substring(1).toLowerCase();
  }

  String capitalizeFirstLetter2(String string) {
    if (string == null) {
      throw ArgumentError("string: $string");
    }

    if (string.isEmpty) {
      return string;
    }

    return string[0].toUpperCase() + string.substring(1);
  }

  bool isKeyboardOpened() {
    return MediaQuery.of(Get.context!).viewInsets.bottom != 0;
  }

  void hideKeyboard(BuildContext context) {
    FocusScope.of(context).requestFocus(new FocusNode());
  }

  void dismissKeyboard(BuildContext context) {
    FocusScopeNode currentFocus = FocusScope.of(context);
    if (!currentFocus.hasPrimaryFocus) {
      currentFocus.unfocus();
    }
  }

  void dismissSnakbar() {
    ScaffoldMessenger.of(Get.context!).hideCurrentSnackBar();
    //ScaffoldMessenger.of(Get.context!).removeCurrentSnackBar();
  }


  bool isCompletedLevel(
      {bool isClaimed = false, int currentLevel = 0, int completedParks = 0}) {
    int remainingParks = ((currentLevel + 1) * 5) - completedParks;
    return remainingParks.isNegative || remainingParks == 0;
  }
  void showSnackBar(String value, {bool isFloat = false}) {
    if (isFloat) {
      ScaffoldMessenger.of(Get.context!).showSnackBar(
        SnackBar(
          duration: Duration(milliseconds: 2000),
          behavior: SnackBarBehavior.floating,
          //backgroundColor: Colors.red,
          content: Text(value, style: TextStyle(color: Colors.white)),
        ),
      );
    } else {
      ScaffoldMessenger.of(Get.context!).showSnackBar(
        SnackBar(
          duration: Duration(milliseconds: 2000),
          content: Text(value),
        ),
      );
    }
  }


}
