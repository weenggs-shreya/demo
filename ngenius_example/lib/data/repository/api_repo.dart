import 'package:dio/dio.dart';
import '../../res/app_constant.dart';
import '../api/api_client.dart';

final title = "ApiRepo";

class ApiRepo {
  ApiClient apiClient = ApiClient();

  Future<Response> getToken() async {
    Map<String, dynamic> params = {
      "Content-Type": "application/vnd.ni-identity.v1+json",
      "Authorization":
          "Basic NGU5OGRiM2UtMTVlYy00Yjk0LWExZTUtODgxZGNlNmJlNjAyOjdhZDVkY2U0LWNkNWEtNDI0ZC1iNWUxLTk2ZWQzN2Q5NTRkYQ=="
    };
    return await apiClient.post(
      url: AppConstant.baseUrl,
      options: Options(
        headers: params,
      ),
    );
  }

  Future<Response> getOrders({required String access_token}) async {
    Map<String, dynamic> header = {
      "Content-Type": "application/vnd.ni-payment.v2+json",
      "Accept": "application/vnd.ni-payment.v2+json",
      "Authorization": "Bearer $access_token"
    };
    Map<String, dynamic> params = {
      "action": "SALE",
      "amount": {"currencyCode": "AED", "value": 100}
    };
    return await apiClient.post(
      url: AppConstant.baseUrlCreateOrder,
      data: params,

      options: Options(
        headers: header,
      ),
    );
  }
}
