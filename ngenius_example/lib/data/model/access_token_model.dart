class AccessTokenModel {
  String? access_token;
  int? expires_in;
  int? refresh_expires_in;
  String? refresh_token;
  String? token_type;

  AccessTokenModel(
      {this.access_token,
      this.expires_in,
      this.refresh_expires_in,
      this.refresh_token,
      this.token_type});

  factory AccessTokenModel.fromJson(Map<String, dynamic> json) =>
      AccessTokenModel(
        access_token:
            json["access_token"] == null ? null : json["access_token"],
        expires_in: json["expires_in"] == null ? null : json["expires_in"],
        refresh_expires_in: json["refresh_expires_in"] == null
            ? null
            : json["refresh_expires_in"],
        refresh_token:
            json["refresh_token"] == null ? null : json["refresh_token"],
        token_type: json["token_type"] == null ? null : json["token_type"],
      );

  Map<String, dynamic> toJson() => {
        "access_token": access_token == null ? null : access_token,
        "expires_in": expires_in == null ? null : expires_in,
        "refresh_expires_in":
            refresh_expires_in == null ? null : refresh_expires_in,
        "refresh_token": refresh_token == null ? null : refresh_token,
        "token_type": token_type == null ? null : token_type,
      };
}
