import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import '../data/api/api_client.dart';
import '../data/common/constants.dart';
import '../data/model/access_token_model.dart';
import '../data/repository/api_repo.dart';

class ApiController extends GetxController {
  ApiRepo apiRepo = ApiRepo();
  AccessTokenModel? accessTokenModel;
  static const channel = MethodChannel('com.example.ngenius_example');

  Future<void> getToken() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.none) {
      constants.showSnackBar(ApiClient().getNetworkError());
      return;
    }
    try {
      final response = await apiRepo.getToken();
      accessTokenModel = AccessTokenModel.fromJson(response.data);
      getOrders();
    } catch (e) {
      print("Error....." + e.toString());
    }
  }

  Future<void> getOrders() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.none) {
      constants.showSnackBar(ApiClient().getNetworkError());
      return;
    }
    try {
      final token = accessTokenModel!.access_token.toString();
      final response = await apiRepo.getOrders(access_token: token);
      var accessToken = await channel.invokeMethod('orderResponse', {
        'response': response.toString(),
      });
      print("accessToken --------------> " + accessToken.toString());
    } catch (e) {
      print("Error....." + e.toString());
    }
  }

  // Future<void> getTokenIOS() async {
  //   var connectivityResult = await (Connectivity().checkConnectivity());
  //   if (connectivityResult == ConnectivityResult.none) {
  //     constants.showSnackBar(ApiClient().getNetworkError());
  //     return;
  //   }
  //   try {
  //     final response = await apiRepo.getToken();
  //     accessTokenModel = AccessTokenModel.fromJson(response.data);
  //
  //
  //     getOrdersIOS();
  //     // getOrders();
  //   } catch (e) {
  //     print("Error....." + e.toString());
  //   }
  // }
  //
  // Future<void> getOrdersIOS() async {
  //   var connectivityResult = await (Connectivity().checkConnectivity());
  //   if (connectivityResult == ConnectivityResult.none) {
  //     constants.showSnackBar(ApiClient().getNetworkError());
  //     return;
  //   }
  //   try {
  //     final token = accessTokenModel!.access_token.toString();
  //     final response = await apiRepo.getOrders(access_token: token);
  //     String? accessToken = await channel.invokeMethod('orderResponse', {
  //       'response': response.toString(),
  //     });
  //     // var accessToken = await channel.invokeMethod('orderResponse', {
  //     //   'response': response.toString(),
  //     // });
  //     print("accessToken --------------> " + accessToken.toString());
  //   } catch (e) {
  //     print("Error....." + e.toString());
  //   }
  // }

}
