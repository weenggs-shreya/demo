import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:ngenius_example/controller/api_controller.dart';
import 'package:ngenius_example/res/strings.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  final ApiController _controller = Get.put(ApiController());
  static const channel = MethodChannel('com.example.ngenius_example');


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(strings.payment_gateway),
      ),
      body: GestureDetector(
        onTap: (){
          
        },
        child: SingleChildScrollView(
          child: Container(
            margin: EdgeInsets.all(10),
            child: Column(
              children: [
                ElevatedButton(
                  onPressed: () {
                    _onpressed();
                  },
                  child: Text("Click"),
                ),
                 ElevatedButton(
                  onPressed: () {
                    _onpressed();
                    // String? result = await channel
                    //     .invokeMethod<String>('isSetUpAvailable', {'text': "hello"});
                    // print("..........."+result.toString());
                  },
                  child: Text("IOS Click"),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future<void> _onpressed() async {
    _controller.getToken();
  }
  // Future<void> _onpressedIOS() async {
  //   print(".....");
  //   _controller.getTokenIOS();
  // }
}
